# Transformers
Welcome to the Transformers Spring Boot App. These instructions will help you to build and run the application as see the API documentation and test using swagger-ui. 

### Java
Java `1.8+` is required.

### Build
Go to the your source folder and run 
```
maven package
```
or alternatively you can also use
```
mvn install
```

### Run
Run this command:
```
 mvn spring-boot:run
```
***Note:** See the application is running using embedded Tomcat on port 8080


### Testing using Swagger-UI

http://localhost:8080/swagger-ui/index.html