package com.aequilibrium.transformers.service;

import java.util.List;

/**
 * Interface exposing service operations
 * 
 * @author andres
 */
public interface TransformerService {
	Transformer create(Transformer transformer);

	Transformer update(String id, Transformer transformer);

	Transformer delete(String id);

	List<Transformer> list();

	GameResults getWinnerTeam(List<String> ids);

}
