package com.aequilibrium.transformers.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.aequilibrium.transformers.Constants.GameState;
import com.aequilibrium.transformers.Constants.Team;

import lombok.Data;

/**
 * Keep an state of the game results
 * 
 * @author andres
 */
@Component
@Data
public class GameResults {
	private List<Transformer> survivors;
	private List<Transformer> skipped;
	private int totalBattles;
	private GameState state;

	@PostConstruct
	private void init() {
		survivors = new ArrayList<Transformer>();
		skipped = new ArrayList<Transformer>();
	}

	public void killAllCompetitors() {
		survivors.clear();
		skipped.clear();
		increaseTotalBattles();
		state = GameState.ENDED;
	}

	public void increaseTotalBattles() {
		totalBattles++;
	}

	public void addSurvivor(Transformer t) {
		survivors.add(t);
		increaseTotalBattles();
	}

	public void addSkippedMembers(List<Transformer> t) {
		skipped.addAll(t);
	}

	public boolean isGameInProgress() {
		return state == GameState.STARTED;
	}

	public Team getWinnerTeam() {
		long autobots = survivors.stream().filter(t -> t.getTeam() == Team.AUTOBOT).count();
		long decepticons = survivors.stream().filter(t -> t.getTeam() == Team.DECEPTICON).count();
		int winnerInt = Integer.compare(Math.toIntExact(autobots), Math.toIntExact(decepticons));
		if (winnerInt > 0) {
			return Team.AUTOBOT;
		} else if (winnerInt < 0) {
			return Team.DECEPTICON;
		}
		state = GameState.TIED;
		return null;
	}

	public List<Transformer> getSurvivorsLossingTeam() {
		List<Transformer> allSurvivors = survivors.stream()
				.filter(t -> t.getTeam() != getWinnerTeam() && state != GameState.TIED)
				.collect(Collectors.toList());
		allSurvivors.addAll(skipped.stream().filter(t -> t.getTeam() != getWinnerTeam() && state != GameState.TIED)
				.collect(Collectors.toList()));
		return allSurvivors;
	}

}
