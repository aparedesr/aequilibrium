package com.aequilibrium.transformers.service;

import static java.util.Objects.isNull;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.aequilibrium.transformers.Constants.GameState;
import com.aequilibrium.transformers.Constants.Team;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
/**
 * Implementation of service for managing tranformer operations
 *
 * @author andres
 */
public class DefaultTranformerService implements TransformerService {
	private Game game;

	@Override
	public Transformer create(Transformer transformer) {
		if (isNull(transformer.getId())) {
			transformer.setId(UUID.randomUUID().toString());
		} else if (game.getTransformers().containsKey(transformer.getId())) {
			return null;
		}
		game.getTransformers().put(transformer.getId(), transformer);
		return transformer;
	}

	@Override
	public Transformer update(String id, Transformer transformer) {
		return game.getTransformers().replace(id, transformer);
	}

	@Override
	public Transformer delete(String id) {
		return game.getTransformers().remove(id);
	}

	@Override
	public List<Transformer> list() {
		return game.getTransformers().values().stream().collect(Collectors.toList());
	}

	@Override
	public GameResults getWinnerTeam(List<String> ids) {
		game.getResults().setState(GameState.STARTED);
		// Split created transformers into both teams
		getTeams(ids);
		// Evaluate the skipped fighters
		getSkippedFighters();
		performBattles();
		return game.getResults();
	}

	/**
	 * Create teams based on specific provided ids. Order teams by desc rank
	 *
	 * @param ids Transformer ids
	 */
	private void getTeams(List<String> ids) {
		List<Transformer> values = ids.stream()
				.map(game.getTransformers()::get)
				.filter(Objects::nonNull) // get rid of null values
				.collect(Collectors.toList());

		values.stream().sorted((p1, p2) -> p2.getRank().compareTo(p1.getRank()))
				.forEach(t -> {
					if (t.getTeam() == Team.AUTOBOT) {
						game.getTeamAutobots().add(t);
					} else {
						game.getTeamDecepticons().add(t);
					}
				});
	}

	/**
	 * Get the skipped fighters based on team sizes
	 */
	private void getSkippedFighters() {
		int memberDifference = game.getTeamAutobots().size() - game.getTeamDecepticons().size();
		if (memberDifference == 0) {
			return;
		} else if (memberDifference > 0) {
			game.getResults().addSkippedMembers(game.getTeamAutobots()
					.subList(game.getTeamAutobots().size() - memberDifference, game.getTeamAutobots().size()));
		} else {
			game.getResults().addSkippedMembers(game.getTeamDecepticons()
					.subList(game.getTeamDecepticons().size() + memberDifference, game.getTeamDecepticons().size()));
		}

	}

	/**
	 * Perform battle validation rules
	 */
	private void performBattles() {
		Iterator<Transformer> t1 = game.getTeamAutobots().iterator();
		Iterator<Transformer> t2 = game.getTeamDecepticons().iterator();
		while (game.getResults().isGameInProgress() && t1.hasNext() && t2.hasNext()) {
			Transformer memberTeam1 = t1.next();
			Transformer memberTeam2 = t2.next();
			// Perform special rules validations
			boolean proceed = executeSpecialRulesInName(memberTeam1, memberTeam2);

			// Perform specific rules around specs
			proceed = (proceed) ? executeOponentHasRanAwayRule(memberTeam1, memberTeam2) : false;
			proceed = (proceed) ? executeSkillAboveRule(memberTeam1, memberTeam2) : false;

			// Evaluate overallrating rule
			proceed = (proceed) ? executeOverallRatingRule(memberTeam1, memberTeam2) : false;
		}
	}

	/**
	 * Execute evaluation around transformer name applying special rules
	 *
	 * @param  memberA, transformer from Autobot team
	 * @param  memberB, transformer from Decepticon team
	 * @return          true if execution continue
	 */
	private boolean executeSpecialRulesInName(Transformer memberA, Transformer memberB) {
		// duplicate of each other or both have special names
		if (memberA.hasSpecialName() && memberB.hasSpecialName()) {
			game.getResults().killAllCompetitors();
		} else if (memberA.hasSpecialName()) {
			game.getResults().addSurvivor(memberA);
		} else if (memberB.hasSpecialName()) {
			game.getResults().addSurvivor(memberB);
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Execute evaluation around skills
	 *
	 * @param  memberA, transformer from Autobot team
	 * @param  memberB, transformer from Decepticon team
	 * @return          true if execution continue
	 */
	private boolean executeSkillAboveRule(Transformer memberA, Transformer memberB) {
		if (memberA.getSkill() - memberB.getSkill() > 3) {
			game.getResults().addSurvivor(memberA);
			return false;
		} else if (memberA.getSkill() - memberB.getSkill() > 3) {
			game.getResults().addSurvivor(memberB);
			return false;
		}
		return true;
	}

	/**
	 * Execute evaluation for ran away rule
	 *
	 * @param  memberA, transformer from Autobot team
	 * @param  memberB, transformer from Decepticon team
	 * @return          true if execution continue
	 */
	private boolean executeOponentHasRanAwayRule(Transformer memberA, Transformer memberB) {
		if ((memberA.getCourage() - memberB.getCourage()) > 4 &&
				((memberA.getStrength() - memberB.getCourage()) > 3)) {
			game.getResults().addSurvivor(memberA);
			return false;
		} else if ((memberB.getCourage() - memberA.getCourage()) > 4 &&
				((memberB.getStrength() - memberA.getCourage()) > 3)) {
			game.getResults().addSurvivor(memberB);
			return false;
		}
		return true;
	}

	/**
	 * Execute evaluation to determinate overall ranking
	 *
	 * @param  memberA, transformer from Autobot team
	 * @param  memberB, transformer from Decepticon team
	 * @return          true if execution continue
	 */
	private boolean executeOverallRatingRule(Transformer memberA, Transformer memberB) {
		int winner = memberA.compareOverallRating(memberB);
		if (winner > 0) {
			game.getResults().addSurvivor(memberA);
			return false;
		} else if (winner < 0) {
			game.getResults().addSurvivor(memberB);
			return false;
		} else {
			game.getResults().increaseTotalBattles();
		}

		return true;
	}

}
