package com.aequilibrium.transformers.service;

import static com.aequilibrium.transformers.Constants.SPECIAL_NAMES;

import java.util.Arrays;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;

import com.aequilibrium.transformers.Constants.Team;

import lombok.Builder;
import lombok.Data;

/**
 * Represent a transformer
 *
 * @author andres
 */
@Data
@Builder
@Validated
public class Transformer {
	private String id;
	@NotEmpty(message = "Name is required")
	private String name;
	@NotNull(message = "Team (AUTOBOT / DECEPTICON) is required")
	private Team team;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int strength;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int intelligence;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int speed;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int endurance;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private Integer rank;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int courage;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int firepower;
	@Range(min = 1, max = 10, message = "Accepted value between 1 and 10")
	private int skill;

	public int getOverallRating() {
		return strength + intelligence + speed + endurance + firepower;
	}

	public boolean hasSpecialName() {
		return Arrays.asList(SPECIAL_NAMES).contains(name);
	}

	public int compareOverallRating(Transformer other) {
		return Integer.compare(getOverallRating(), other.getOverallRating());
	}

}
