package com.aequilibrium.transformers.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class Game {
	private Map<String, Transformer> transformers;
	private List<Transformer> teamAutobots;
	private List<Transformer> teamDecepticons;
	@Autowired
	private GameResults results;

	@PostConstruct
	private void init() {
		transformers = new HashMap<>();
		teamAutobots = new ArrayList<>();
		teamDecepticons = new ArrayList<>();
	}
}
