package com.aequilibrium.transformers;

public class Constants {
	public final static String ROOT_API_URL_TRANSFORMERS = "/api/v1/transformers";
	public final static String WINNER_ACTION = "/winner";
	public final static String SPEC_INVALID_VALUE = "Accepted value between 1 and 10";
	public final static String INVALID_NAME = "Name is required";
	public final static String INVALID_ID = "ID is required";
	public final static String[] SPECIAL_NAMES = { "Predaking", "Optimus Prime" };

	public enum Team {
		AUTOBOT, DECEPTICON
	}

	public enum GameState {
		STARTED, ENDED, TIED
	}
}
