package com.aequilibrium.transformers.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String message;
}
