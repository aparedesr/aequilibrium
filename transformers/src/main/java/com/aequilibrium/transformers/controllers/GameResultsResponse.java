package com.aequilibrium.transformers.controllers;

import java.util.List;

import com.aequilibrium.transformers.Constants.Team;
import com.aequilibrium.transformers.service.Transformer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameResultsResponse {
	private int totalBattles;
	private List<Transformer> survivorsLossingTeam;
	private Team winnerTeam;
}
