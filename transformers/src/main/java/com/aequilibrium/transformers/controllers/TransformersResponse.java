package com.aequilibrium.transformers.controllers;

import java.util.List;

import com.aequilibrium.transformers.service.Transformer;

import lombok.Data;

@Data
public class TransformersResponse {
	private List<Transformer> transformers;

}
