package com.aequilibrium.transformers.controllers;

import static com.aequilibrium.transformers.Constants.ROOT_API_URL_TRANSFORMERS;
import static com.aequilibrium.transformers.Constants.WINNER_ACTION;
import static java.util.Objects.isNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.aequilibrium.transformers.service.GameResults;
import com.aequilibrium.transformers.service.Transformer;
import com.aequilibrium.transformers.service.TransformerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author andres
 */
@RestController
@RequestMapping(value = ROOT_API_URL_TRANSFORMERS)
@Api(value = "TransformersController", description = "Operations for managing transformers and getting team winner")
public class TransformersController {

	@Autowired
	private TransformerService service;

	@PostMapping("/")
	@ApiOperation(value = "Create a new transformer", response = Transformer.class, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Transformer> create(
			@ApiParam(required = true, name = "transformer", value = "Transformer to be created") @RequestBody @Validated Transformer transformer)
			throws URISyntaxException {
		transformer = service.create(transformer);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(transformer.getId())
				.toUri();
		if (isNull(transformer)) {
			return ResponseEntity.badRequest().build();
		} else {
			return ResponseEntity.created(uri)
					.body(transformer);
		}
	}

	@GetMapping("/")
	@ApiOperation(value = "List all the existing transformers", response = TransformersResponse.class, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TransformersResponse> list() {
		TransformersResponse response = new TransformersResponse();
		response.setTransformers(service.list());
		return ResponseEntity.ok(response);
	}

	@PutMapping("/{transformerId}")
	@ApiOperation(value = "Update a transformer", response = Transformer.class)
	public ResponseEntity<Transformer> update(
			@ApiParam(required = true, name = "transformerId", value = "Transformer Id") @PathVariable String transformerId,
			@ApiParam(required = true, name = "transformer", value = "Transformer") @RequestBody Transformer transformer) {
		if (isNull(service.update(transformerId, transformer))) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(transformer);
		}
	}

	@DeleteMapping("/{transformerId}")
	@ApiOperation(value = "Delete a transformer")
	public ResponseEntity<?> delete(
			@ApiParam(required = true, name = "transformerId", value = "Transformer Id") @PathVariable String transformerId) {
		if (isNull(service.delete(transformerId))) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@GetMapping(WINNER_ACTION)
	@ApiOperation(value = "Determine a winning team", response = GameResultsResponse.class, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> winnerTeam(
			@ApiParam(required = true, name = "transformerIds", value = "Transformer Ids") @RequestParam List<String> transformerIds) {
		GameResults results = service.getWinnerTeam(transformerIds);
		GameResultsResponse response = new GameResultsResponse(results.getTotalBattles(),
				results.getSurvivorsLossingTeam(), results.getWinnerTeam());
		return ResponseEntity.ok(response);
	}
}
