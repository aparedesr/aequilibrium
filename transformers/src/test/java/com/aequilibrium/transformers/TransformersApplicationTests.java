package com.aequilibrium.transformers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.aequilibrium.transformers.Constants.Team;
import com.aequilibrium.transformers.service.Game;
import com.aequilibrium.transformers.service.Transformer;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class TransformersApplicationTests {
	private Game game;
	@Autowired
	private MockMvc mvc;
	@Autowired
	private ObjectMapper objectMapper;
	private Transformer t = Transformer.builder()
			.id("1")
			.rank(1)
			.team(Team.AUTOBOT)
			.name("Bob Auto")
			.skill(3)
			.courage(1)
			.endurance(4)
			.firepower(5)
			.intelligence(2)
			.speed(3)
			.strength(3)
			.build();
	private Transformer t2 = Transformer.builder()
			.id("2")
			.rank(10)
			.team(Team.AUTOBOT)
			.name("Optimus Prime")
			.skill(8)
			.courage(9)
			.endurance(9)
			.firepower(9)
			.intelligence(5)
			.speed(7)
			.strength(10)
			.build();
	private Transformer t3 = Transformer.builder()
			.id("3")
			.rank(9)
			.team(Team.DECEPTICON)
			.name("Mega Auto")
			.skill(8)
			.courage(6)
			.endurance(4)
			.firepower(3)
			.intelligence(7)
			.speed(4)
			.strength(6)
			.build();
	private Transformer t4 = Transformer.builder()
			.id("4")
			.rank(4)
			.team(Team.DECEPTICON)
			.name("Big Auto")
			.skill(6)
			.courage(5)
			.endurance(2)
			.firepower(3)
			.intelligence(9)
			.speed(2)
			.strength(5)
			.build();
	private Transformer t1 = Transformer.builder()
			.id("2")
			.rank(10)
			.team(Team.DECEPTICON)
			.name("Predaking")
			.skill(8)
			.courage(9)
			.endurance(9)
			.firepower(9)
			.intelligence(5)
			.speed(7)
			.strength(10)
			.build();

	@BeforeAll
	public static void before() {
	}

	@Test
	void whenInputIsInvalid_thenReturnsStatus400() throws Exception {
		String body = "{}";

		mvc.perform(post("/api/v1/transformers/")
				.contentType("application/json")
				.content(body))
				.andExpect(status().isBadRequest());
	}

	@Test
	void whenInputIsInvalid_thenReturnsStatus200() throws Exception {
		String body = "{}";

		mvc.perform(get("/api/v1/transformers/")
				.contentType("application/json")
				.content(body))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void postValidRequestTransformer() throws Exception {
		String body = objectMapper.writeValueAsString(t);

		mvc.perform(post("/api/v1/transformers/")
				.contentType("application/json")
				.content(body))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void postBadRequestTransformer() throws Exception {
		Transformer e = Transformer.builder()
				.rank(1)
				.team(Team.AUTOBOT)
				.name("Name1").build();
		String body = objectMapper.writeValueAsString(e);

		mvc.perform(post("/api/v1/transformers/")
				.contentType("application/json")
				.content(body))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void getAllTransformers() throws Exception {
		postTransformer(t);
		mvc.perform(MockMvcRequestBuilders
				.get("/api/v1/transformers/")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.transformers").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.transformers[*].id").isNotEmpty());
	}

	@Test
	public void NotFoundPutRequestTransformer() throws Exception {
		postTransformer(t);
		String body = objectMapper.writeValueAsString(t);

		mvc.perform(put("/api/v1/transformers/222222")
				.contentType("application/json")
				.param("transformerId", "222222")
				.content(body))
				.andExpect(status().isNotFound());
	}

	@Test
	public void validPutRequestTransformer() throws Exception {
		postTransformer(t);
		String body = objectMapper.writeValueAsString(t);

		mvc.perform(put("/api/v1/transformers/1")
				.contentType(MediaType.APPLICATION_JSON)
				.param("transformerId", "1")
				.content(body))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void validDeleteRequestTransformer() throws Exception {
		postTransformer(t);
		mvc.perform(delete("/api/v1/transformers/1")
				.contentType(MediaType.APPLICATION_JSON)
				.param("transformerId", "1"))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void validGetWinner() throws Exception {
		postTransformer(t);
		postTransformer(t3);
		postTransformer(t4);
		mvc.perform(MockMvcRequestBuilders
				.get("/api/v1/transformers/winner")
				.accept(MediaType.APPLICATION_JSON)
				.param("transformerIds", "1")
				.param("transformerIds", "2")
				.param("transformerIds", "4"))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalBattles").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.winnerTeam").isNotEmpty());
	}

	@Test
	public void noWinnerAndAllKilled() throws Exception {
		postTransformer(t);
		postTransformer(t2); // Optimus
		postTransformer(t1); // Predaking
		mvc.perform(MockMvcRequestBuilders
				.get("/api/v1/transformers/winner")
				.accept(MediaType.APPLICATION_JSON)
				.param("transformerIds", "1")
				.param("transformerIds", "2")
				.param("transformerIds", "4"))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalBattles").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.winnerTeam").doesNotExist());
	}

	@Test
	public void validGetWinnerOptimus() throws Exception {
		postTransformer(t);
		postTransformer(t2); // Optimus
		postTransformer(t3);
		mvc.perform(MockMvcRequestBuilders
				.get("/api/v1/transformers/winner")
				.accept(MediaType.APPLICATION_JSON)
				.param("transformerIds", "1")
				.param("transformerIds", "2")
				.param("transformerIds", "3"))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalBattles").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.winnerTeam").isNotEmpty());
	}

	private void postTransformer(Transformer transformer) throws Exception {
		String body = objectMapper.writeValueAsString(transformer);

		mvc.perform(delete("/api/v1/transformers/" + transformer.getId())
				.contentType("application/json")
				.param("transformerId", transformer.getId()));

		mvc.perform(post("/api/v1/transformers/")
				.contentType("application/json")
				.content(body));
	}

}
